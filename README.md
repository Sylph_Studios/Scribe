# Scribe
A fast, bi-modal IDE written primarily in Rust.

## Introduction
My goal for Scribe is a simple yet effective and easy to use IDE, with the ability for community supported language features,\
with Rust being Scribe's Native language, which would come pre-configured out of the box.